# catastrophe-world

## Proposal
Our goal is to promote relief for victims of natural disasters throughout the world, as well as, spread awareness to
different organizations that work for these victims. Furthermore, we hope to educate our audience about different natural disasters and the locations they occur in. Millions of people are affected by natural disasters but we only often hear about ones that affect those near us. Our objective is to support the recovery efforts and provide some relief to victims of these catastrophes.

## API URLs
ReliefWeb: https://reliefweb.int/help/api <br />
Wikipedia: https://www.mediawiki.org/wiki/API:Main_page <br />
PredictHQ: https://www.predicthq.com/events/disasters <br />
GuideStar: https://learn.guidestar.org/products/business-solutions/guidestar-apis <br />
FEMA: https://www.fema.gov/openfema-api-documentation <br />
National Weather Service: https://www.weather.gov/documentation/services-web-api <br />

## Models
Countries: each country has its location (may be broken up further into states), population, number of people affected by disasters, money spent on disaster relief, maps

Disasters: locations, damage/clean up costs, specific dates and times, type/category, maps, images of the aftermath

Organizations: headquarter locations, description, funding, locations served, website
