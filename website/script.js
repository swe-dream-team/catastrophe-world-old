function load_gitlab_data() {
    var xhttp = new XMLHttpRequest();

    var preston = 0;
    var allen = 0;
    var david = 0;
    var alex = 0;
    var chandler = 0;

    xhttp.onreadystatechange = function() {
         if (this.readyState == 4 && this.status == 200) {
          var commits = JSON.parse(this.responseText);
          for (var index in commits) {
            switch(commits[index].committer_name) {
              case "chandleryoon":
                chandler++;
                break;
              case "Preston Bao":
                preston++;
                break;
              case "Allen Wang":
                allen++;
                break;
              case "David Shiu":
                david++;
                break;
              case "Alex":
                alex++;
                break;
              default:
                break;
            }
          }

          document.getElementById("preston-commits").innerHTML = "Commits: " + preston;
          document.getElementById("allen-commits").innerHTML = "Commits: " + allen;
          document.getElementById("david-commits").innerHTML = "Commits: " + david;
          document.getElementById("alex-commits").innerHTML = "Commits: " + alex;
          document.getElementById("chandler-commits").innerHTML = "Commits: " + chandler;

         }
    };
    xhttp.open("GET", " https://gitlab.com/api/v4/projects/10986075/repository/commits?per_page=100", true);
    xhttp.setRequestHeader("Content-type", "application/json");
    xhttp.send();
}